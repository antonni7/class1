package com.example.class1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.class1.model.KotaModel;

public interface KotaRepository extends JpaRepository<KotaModel, String>{
	
	@Query("SELECT MAX(K.idKota) FROM KotaModel K")
	Integer searchMaxId();
	
	@Query("SELECT K FROM KotaModel K WHERE K.kodeKota = ?1")
	KotaModel searchKodeKota(String kodeKota);

}
